
/**
 * Module dependencies.
 */
require('./routes/axonPortConfig');
require('./config');

productTypeInfoList = [];
productEPClist = [];


var colors = require('colors');

colors.setTheme({
  silly: 'rainbow',
  input: 'grey',
  verbose: 'cyan',
  prompt: 'grey',
  info: 'green',
  data: 'grey',
  help: 'cyan',
  warn: 'yellow',
  debug: 'blue',
  error: 'red'
});
var EventProxy         = require('eventproxy');
g_EventProxy = new EventProxy();

var Q                  = require('q');
var Datastore          = require('nedb');

var productCurrentStateDB  = new Datastore({ filename: 'productCurrentState.db', autoload: true });


productCurrentStateDBFind = Q.nbind(productCurrentStateDB.find, productCurrentStateDB);
productCurrentStateDBRemove = Q.nbind(productCurrentStateDB.remove, productCurrentStateDB);
productCurrentStateDBInsert = Q.nbind(productCurrentStateDB.insert, productCurrentStateDB);


var bagageDB = new Datastore({ filename: 'bagage.db', autoload: true });
bagageDBFind = Q.nbind(bagageDB.find, bagageDB);
bagageDBRemove = Q.nbind(bagageDB.remove, bagageDB);
bagageDBInsert = Q.nbind(bagageDB.insert, bagageDB);

var orderDB = new Datastore({ filename: 'order.db', autoload: true });
orderDBFind = Q.nbind(orderDB.find, orderDB);
orderDBRemove = Q.nbind(orderDB.remove, orderDB);
orderDBInsert = Q.nbind(orderDB.insert, orderDB);
orderDBUpdate = Q.nbind(orderDB.update, orderDB);


var request = require('request');
httpRequestGet = Q.denodeify(request.get);
httpRequestPost = Q.denodeify(request.post);

var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');
var productCurrentState = require('./routes/productCurrentState');
var bagageInfo = require('./routes/bagageInfo');
var orderInfo = require('./routes/orderInfo');

var app = express();

// all environments
app.set('port', process.env.PORT || httpListeningPort);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}
app.get('/', routes.index);
app.get('/indexapi', routes.indexAPI);
app.get('/overview', routes.overview);
app.post('/productCurrentStateWithSegmentExpand', routes.productCurrentStateWithSegmentExpand);

// app.get('/productListIndex', productCurrentState.productListIndex);
app.post('/productCurrentStateList', productCurrentState.statelist);
app.post('/updateProductState', productCurrentState.updateProductState);
app.get('/testUpdateProductState', productCurrentState.testUpdateProductState)
app.post('/productCurrentStateWithSegment', productCurrentState.productCurrentStateWithSegment);

app.get('/testAddBagageInfo', bagageInfo.testAddBagageInfo);
app.post('/allBagageList', bagageInfo.allBagageList);
app.post('/addBagageInfo', bagageInfo.addBagageInfo);
app.post('/removeBagageInfo', bagageInfo.removeBagageInfo);
app.post('/specialBagageInfo', bagageInfo.specialBagageInfo);
app.post('/bagageInfoWithEPC', bagageInfo.bagageInfoWithEPC);

app.get('/testAddOrder', orderInfo.testAddOrder);
app.post('/orderListAll', orderInfo.orderListAll);
app.post('/addOrder', orderInfo.addOrder);
app.post('/removeOrder', orderInfo.removeOrder);
app.post('/updateOrder', orderInfo.updateOrder);
app.post('/orderListOfSpecifiedSupplier', orderInfo.orderListOfSpecifiedSupplier);
app.post('/orderListOfSpecifiedOrders', orderInfo.orderListOfSpecifiedOrders);



http.createServer(app).listen(app.get('port'), function(){
  console.log(('Logis Info server listening on port ' + app.get('port')).info);
});
