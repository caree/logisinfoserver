

var _       = require('underscore');
var request = require('request');

var productCurrentState = require('./productCurrentState');


g_EventProxy.tail('ProductChanged', __refreshProductInfo);
g_EventProxy.tail('ProductTypeChanged', __refreshProductTypeInfo);


require('./communication');
requestBaseData();

exports.indexAPI = function(req, res){
	res.render('indexAPI', {title:'API Test'});
}
exports.index = function(req, res){
	res.render('index', {title:'物流信息管理系统', _system_name:'物流信息管理系统', _company:'北京科技发展有限公司'});
}
exports.overview = function(req, res){
	res.render('overview');
};

function requestBaseData(){
	console.log('requestBaseData ...'.data);
	__refreshProductInfo();
	__refreshProductTypeInfo();
}
function __refreshProductTypeInfo(){
	httpRequestPost(epcServerHost + 'listTypeInfo', {}).then(function(_response){
		var data = _response[0].body;
		// console.log(data);
		try{
			var list = JSON.parse(data);
			productTypeInfoList = list;
			console.log('productType updated!!'.info);
			console.dir(productTypeInfoList);	
		}catch(e){
			console.log(data.error);
		}
	}).catch(function(error){
		console.log('error <= requestBaseData listTypeInfo'.error);
	});
}
function __refreshProductInfo(){
	httpRequestPost(epcServerHost + 'productlist', {}).then(function(_response){
		var data = _response[0].body;
		// console.log(data);
		try{
			var list = JSON.parse(data);
			productEPClist = list;
			console.log('productEPClist updated!!'.info);
			console.dir(productEPClist);	
		}catch(e){
			console.log(data.error);
		}
	}).catch(function(error){
		console.log('error <= requestBaseData productlist'.error);
	});	
}
 // productName  产品类别名称
 // EPC          产品EPC
 // Segment      环节
 // time         时间       
 // state        返回状态
exports.productCurrentStateWithSegmentExpand = function(req, res){
	var segment = req.body.segment;
	if(segment == null){
		res.send('paraError'); return;
	}
	productCurrentState.statelistAll({segment: segment})
	.then(function(_list){
		var list = _.map(_list, function(_productState){
			var EPCcodeMap = _.findWhere(productEPClist, {productEPC: _productState.productEPC});
			if(EPCcodeMap == null) return null;
			var typeCode = EPCcodeMap.productCode;
			var typeInfo = _.findWhere(productTypeInfoList, {productCode: typeCode});
			if(typeInfo == null) return null;
			_productState.productCode = typeInfo.productCode;
			_productState.productName = typeInfo.productName;
			return _productState;
		});
		var str = JSON.stringify(_.without(list, null));
		console.log(str.data);
		res.send(str);
	}).catch(function(error){
		console.log(('error <= productCurrentStateWithSegmentExpand' + error.message).error);
		res.send(error.message);
	})	
}
//*********************************************************
