
var timeFormater = require('./timeFormat').getCurrentTime;
var _                  = require('underscore');
var Q                  = require('q');


// { epc: '101', itemType: '01', collection: 'epc'}
// {productCode:"01",productName:"电脑", collection: 'itemType'}


function bagageInfo(_bagageID, _productEPC){
	this.bagageID = _bagageID;
	this.productEPC = _productEPC;
	this.timeStamp = timeFormater();
}
bagageInfo.prototype.tryValidate = function() {
	if(this.bagageID == null || this.bagageID.length <=0 
		|| this.productEPC == null || this.productEPC.length <= 0){
		console.log(('bagageInfo tryValidate error').error);
		return false;
	}else{
		return true;
	} 
};

exports.allBagageList = function(req, res){
	bagageDBFind({})
	.then(function(_list){
		var bagageIDs = _.uniq(_.pluck(_list, "bagageID"));
		// var bagageList = _.map(_list, function(_bagage){
		// 	return {bagageID: _bagage.bagageID, timeStamp: _bagage.timeStamp};
		// })
		var str = JSON.stringify(bagageIDs);
		console.log(str.data);
		res.send(str);
	}).catch(function(error){
		console.log(('error <= allBagageList' + error.message).error);
		res.send(error.message);
	})
};
exports.specialBagageInfo = function(req, res){
	var bagageID = req.body.bagageID;
	bagageDBFind({bagageID: bagageID})
	.then(function(_list){
		var str = JSON.stringify(_list);
		console.log(str.data);
		res.send(str);
	}).catch(function(error){
		console.log(('error <= allBagageList  ' + error.message).error);
		res.send(error.message);
	})
};
exports.addBagageInfo = function(req, res){
	var body = req.body;
	var bagageID = body.bagageID;
	var productEPC = body.productEPC;

	_addBagageInfo(bagageID, productEPC).then(function(){
		res.send('ok');
	}).catch(function(error){
		console.log(('error <= addBagageInfo' + error.message).error);
		res.send(error.message);
	})
}
exports.removeBagageInfo = function(req, res){
	var bagageID = req.body.bagageID;
	bagageDBRemove({bagageID: bagageID}, { multi: true }).then(function(_number){
		if(_number <= 0){
			res.send('noData');
		}else{
			res.send('ok');
		}
	}).catch(function(error){
		console.log(('error <= removeBagageInfo ' + error.message).error);
		res.send('error');
	})
}
exports.bagageInfoWithEPC = function(req, res){
	var productEPC = req.body.productEPC;
	bagageDBFind({productEPC: productEPC})
	.then(function(_list){
		if(_.size(_list) > 0){
			var str = JSON.stringify(_list[0]);
			console.log(str.data);
			res.send(str);
		}else{
			res.send('');
		}
	}).catch(function(error){
		console.log(('error <= bagageInfoWithEPC ' + error.message).error);
		res.send(error.message);
	})
}
exports.removeEPCinBagage = function(req, res){
	var productEPC = req.body.productEPC;
	var bagageID = req.body.bagageID;
	bagageDBRemove({bagageID: bagageID, productEPC: productEPC})
	.then(function(_number){
		if(_number > 0){
			res.send('ok');
		}else{
			res.send('noData');
		}
	}).catch(function(error){
		console.log('error <= removeEPCinBagage ' + error.message);
		res.send(error.message);
	})
}
exports.testAddBagageInfo = function(req, res){
	_addBagageInfo('b000002', '000003')
	.then(function(){
		res.send('ok');
	}).catch(function(error){
		console.log(('error <= testAddBagageInfo ' + error.message).error);
		res.send('error');
	})
}
function _addBagageInfo(_bagageID, _productEPC){
	var paraNew = new bagageInfo(_bagageID, _productEPC);
	console.dir(paraNew);
	if(paraNew.tryValidate()){
		return bagageDBFind({productEPC: paraNew.productEPC}).then(function(_list){
			if(_.size(_list) > 0){
				console.dir(_list);
				throw new Error('duplicated');
			}else{
				return bagageDBInsert(paraNew);
			}
		})
	}else{
		return Q.fcall(function(){
			throw new  Error('paraError');	
		})
	}
}

//****************************************************************************
