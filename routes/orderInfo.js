
var timeFormater = require('./timeFormat').getCurrentTime;
var _                  = require('underscore');
var Q                  = require('q');


	// orderID      订单ID
	// productCode  产品代码
	// orders       订货商
	// Supplier     供货商
	// quantity     数量
	// timeStamp    时间
	// OrderState   订单状态
	// note         备注

function orderInfo(_orderID, _productCode, _orders, _supplier, _quantity, _orderState, _note){
	this.orderID = _orderID;
	this.productCode = _productCode;
	this.orders = _orders;
	this.supplier = _supplier;
	this.quantity = _quantity;
	this.orderState = (_orderState == null) ? "" : _orderState;
	this.note = (_note == null) ? "" : _note;
	this.timeStamp = timeFormater();
}
orderInfo.prototype.tryValidate = function() {
	if(this.orderID == null || this.orderID.length <=0
		|| this.productCode == null || this.productCode.length <= 0
		|| this.orders == null || this.orders.length <= 0
		|| this.supplier == null || this.supplier.length <= 0
		|| this.quantity == null || this.quantity.length <= 0){
		console.log(('orderInfo tryValidate error').error);
		return false;
	}else{
		return true;
	} 
};
function addProductNameToList(_list){
	return _.chain(_list).map(function(_order){
				var typeInfo = _.chain(productTypeInfoList).findWhere({productCode: _order.productCode}).value();
				if(typeInfo != null){
					_order.productName = typeInfo.productName;
				}else{
					_order.productName = "未定义";
				}
				return _order;
			}).value();
}

exports.orderListAll = function(req, res){
	var supplier = req.body.supplier;
	var orders = req.body.orders;

	orderDBFind(initialSelector({orders: orders, supplier: supplier}))
	.then(function(_list){
		var list = addProductNameToList(_list);
		var str = JSON.stringify(list);
		console.log(str.data);
		res.send(str);
	}).catch(function(error){
		console.log(('error <= orderListAll' + error.message).error);
		res.send(error.message);
	})	
};
function initialSelector(opts){
	if(opts == null) return {};
	var obj = {};
	_.chain(opts).keys().reduce(function(memo, _key){
		if(opts[_key] != null){
			memo[_key] = opts[_key];
		}
	}, obj).value();
	return obj;
}

exports.orderListOfSpecifiedOrders = function(req ,res){
	var orders = req.body.orders;
	if(orders == null){
		res.send('paraError');return;
	}
	orderDBFind({orders: orders})
	.then(function(_list){
		var list = addProductNameToList(_list);
		var str = JSON.stringify(list);
		console.log(str.data);
		res.send(str);
	}).catch(function(error){
		console.log(('error <= orderListOfSpecifiedOrders' + error.message).error);
		res.send(error.message);
	})	
}
exports.orderListOfSpecifiedSupplier = function(req, res){
	var supplier = req.body.supplier;
	if(supplier == null){
		res.send('paraError');return;
	}
	orderDBFind({supplier: supplier})
	.then(function(_list){
		var list = addProductNameToList(_list);
		var str = JSON.stringify(list);
		console.log(str.data);
		res.send(str);
	}).catch(function(error){
		console.log(('error <= orderListOfSpecifiedSupplier' + error.message).error);
		res.send(error.message);
	})		
}
exports.addOrder = function(req, res){
	var body = req.body;
	var orderID = body.orderID;
	var productCode = body.productCode;
	var orders = body.orders;
	var supplier = body.supplier;
	var quantity = body.quantity;
	var orderState = body.orderState;
	var note = body.note;

	_addOrder(orderID, productCode, orders, supplier, quantity, orderState, note).then(function(){
		res.send('ok');
	}).catch(function(error){
		console.log(('error <= addOrder' + error.message).error);
		res.send(error.message);
	})
}
exports.removeOrder = function(req, res){
	var orderID = req.body.orderID;
	orderDBRemove({orderID: orderID}).then(function(_number){
		if(_number <= 0){
			res.send('noData');
		}else{
			res.send('ok');
		}
	}).catch(function(error){
		console.log(('error <= removeAction ' + error.message).error);
		res.send('error');
	})
}
exports.updateOrder = function(req, res){
	var orderID = req.body.orderID;
	var orderState = req.body.orderState;
	if(orderID == null || orderID.length <= 0 
		|| orderState == null || orderState.length <= 0){
		res.send('paraError');
		return;
	}
	orderDBUpdate({orderID: orderID}, {$set:{orderState: orderState}})
	.then(function(_nubmer){
		if(_nubmer > 0){
			res.send('ok');
		}else{
			res.send('noData');
		}
	}).catch(function(error){
		console.log(('error <= testAddAction' + error.message).error);
		res.send(error.message);
	})
}
exports.testAddOrder = function(req, res){
	_addOrder('O000001', 'p00001', 'orderman', 'supplierman', '10')
	.then(function(){
		res.send('ok');
	}).catch(function(error){
		console.log(('error <= testAddAction' + error.message).error);
		res.send('error');
	})
}
function _addOrder(_orderID, _productCode, _orders, _supplier, _quantity, _orderState, _note){
	var orderNew = new orderInfo(_orderID, _productCode, _orders, _supplier, _quantity, _orderState, _note);
	// console.dir(orderNew);
	if(orderNew.tryValidate()){
		return orderDBFind({orderID: orderNew.orderID}).then(function(_list){
			if(_.size(_list) > 0){
				return orderDBRemove({orderID: orderNew.orderID})
				.then(function(_number){
					return orderDBInsert(orderNew);
				})
			}else{
				return orderDBInsert(orderNew);
			}
		})
	}else{
		return Q.fcall(function(){
			throw new  Error('paraError');	
		})
	}
}

//****************************************************************************
