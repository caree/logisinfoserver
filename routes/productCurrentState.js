
var timeFormater = require('./timeFormat').getCurrentTime;
var _                  = require('underscore');
var Q                  = require('q');


// { epc: '101', itemType: '01', collection: 'epc'}
// {productCode:"01",productName:"电脑", collection: 'itemType'}


function productState(_productEPC, _segment, _note){
	this.productEPC = _productEPC;
	this.segment = _segment;
	this.timeStamp = timeFormater();
	if(_note == null){
		this.note = "";
	}else
		this.note = _note;
}
productState.prototype.tryValidate = function() {
	if(this.productEPC == null || this.productEPC.length <=0 
		|| this.segment == null || this.segment.length <= 0){
		console.log(('productState tryValidate error').error);
		return false;
	}else{
		return true;
	} 
};
exports.statelistAll = function(_selector){
	if(_selector == null) _selector = {};
	return productCurrentStateDBFind(_selector);
}
exports.statelist = function(req, res){
	var productEPC = req.body.productEPC;
	productCurrentStateDBFind({productEPC: productEPC})
	.then(function(_list){
		var str = JSON.stringify(_list);
		console.log(str.data);
		res.send(str);
	}).catch(function(error){
		console.log(('error <= statelist' + error.message).error);
		res.send(error.message);
	})
};
exports.productCurrentStateWithSegment = function(req, res){
	var segment = req.body.segment;
	productCurrentStateDBFind({segment: segment})
	.then(function(_list){
		var str = JSON.stringify(_list);
		console.log(str.data);
		res.send(str);
	}).catch(function(error){
		console.log(('error <= productCurrentStateWithSegment' + error.message).error);
		res.send(error.message);
	})	
}

exports.updateProductState = function(req, res){
	var body = req.body;
	// console.dir(body);
	var productEPC = body.productEPC;
	var segment = body.segment;
	var note = body.note;
	if(_.findWhere(productEPClist, {productEPC: productEPC}) == null){
		console.log('no such productEPC'.error);
		res.send('noSuchProductEPC');
		return;
	}
	_updateProductState(productEPC, segment, note).then(function(){
		res.send('ok');
	}).catch(function(error){
		console.log(('error <= updateProductState' + error.message).error);
		res.send(error.message);
	})
}

exports.testUpdateProductState = function(req, res){
	_updateProductState('000001', 'inventory', 'inventory test')
	.then(function(){
		res.send('ok');
	}).catch(function(error){
		console.log('error <= testUpdateProductState'.error);
		console.dir(error);
		res.send('error');
	})
}
function _updateProductState(_productEPC, _segment, _note){
	var stateNew = new productState(_productEPC, _segment, _note);
	console.dir(stateNew);
	if(stateNew.tryValidate()){
		return productCurrentStateDBFind({productEPC: stateNew.productEPC}).then(function(_list){
			if(_.size(_list) > 0){
				return productCurrentStateDBRemove({productEPC: stateNew.productEPC})
				.then(function(_number){
					return productCurrentStateDBInsert(stateNew);
				})
			}else{
				return productCurrentStateDBInsert(stateNew);
			}
		})
	}else{
		return Q.fcall(function(){
			throw new  Error('paraError');	
		})
	}
}

//****************************************************************************

